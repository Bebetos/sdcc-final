# SDCC Project #
## A serverless application for smart cars ##

This repository contains all the files necessary for a fast deploy on AWS.
<br>
Inside Lambda folder there are the .yaml files containing the option necessary for the execution and the .zip files the code to execute.
<br>
Inside API Gateway folder there are the .yaml files containings the API Gateways for used by the serverless web application.


### Code repository ###
* Front-End: The repository is available at this link: https://gitlab.com/Bebetos/sdcc-dashboard <br>
* Python Code for AWS Lambda and Kinesis at this link: https://gitlab.com/Bebetos/sdcc-code.git

### Developer ###
* Alberto Talone